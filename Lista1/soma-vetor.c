#include <stdio.h>

void read_vector(int v[], int n) {
    int i;
    for (i = 0; i < n; i++)
        scanf("%d", &v[i]);
}

void show_vector(int v[], int n) {
    int i;
    for (i = 0; i < n; i++)
        printf("| %d ", v[i]);
    printf("|\n");
}

int sum_vector(int v[], int a, int b) {
    if (a == b)
        return v[a];
    return v[b] + sum_vector(v, a, b - 1);
}

int main() {
    int n;
    printf("informe o tamanho do vetor: ");
    scanf("%d", &n);
    int v[n];
    read_vector(v, n);
    show_vector(v, n);
    printf("soma do vetor: %d\n\n",sum_vector(v, 0, n-1));
    return 0;
}
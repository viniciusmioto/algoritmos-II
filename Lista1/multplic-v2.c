#include <stdio.h>
#include <math.h>

int mult(int a, int b) {
    if (b == 0) {
        return 0;
    }
    if (b % 2 == 0) 
        return mult(a + a, ceil(b/2));
    return mult(a+a, ceil(b/2)) + a;
}

int main() {
    int a, b;
    printf("informe A: ");
    scanf("%d", &a);
    printf("informe B: ");
    scanf("%d", & b);
    printf("Saida  =  %d\n\n", mult(a, b));
    return 0;
}

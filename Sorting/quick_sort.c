# include <stdio.h>

void swap(int array[], int a, int b) {
    int aux;
    aux = array[a];
    array[a] = array[b];
    array[b] = aux;
}

int partition(int array[], int start, int end, int pivot) {
    int m = start - 1;
    int i;
    for (i = start; i <= end; i++) 
        if (array[i] <= pivot) {
            m++;
            swap(array, m, i);
        }
    
    return m;
}

void quick_sort(int array[], int start, int end) {
    int pivot;
    
    if (start >= end)
        return ;
    
    pivot = partition(array, start, end, array[end]);

    quick_sort(array, start, pivot - 1);
    quick_sort(array, pivot + 1, end);
}

void print_array(int array[], int n) {
    int i;
    for (i = 0; i < n; i++)
        printf("| %d ", array[i]);

    printf("|\n");
}

int main () {
    int array[10] = {55, 32, 67, 28, 22, 98, 13, 26, 75, 72};
    int length = sizeof(array)/sizeof(array[0]);
    print_array(array, length);
    printf("Quick Sort\n");
    quick_sort(array, 0, length - 1);
    print_array(array, length);
    return 0;
}
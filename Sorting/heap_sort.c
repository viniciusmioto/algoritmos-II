# include <stdio.h>
# include <math.h>

void swap(int array[], int a, int b) {
    int aux;
    aux = array[a];
    array[a] = array[b];
    array[b] = aux;
}

void print_array(int array[10], int n) {
    int i;
    for (i = 0; i < n; i++)
        printf("| %d ", array[i]);

    printf("|\n");
}

void max_heapify(int array[], int length, int i) {
    int left = 2 * i + 1;
    int right = 2 * i + 2;
    int largest = i;

    if (left < length && array[left] > array[largest]) 
        largest = left;

    if (right < length && array[right] > array[largest])
        largest = right;

    if (largest != i) {
        swap(array, i, largest);
        max_heapify(array, length, largest);
    }
}

void build_max_heap(int array[], int length) {
    int i;
    
    for (i = length / 2 - 1; i >= 0; i--) 
        max_heapify(array, length, i);
}

void heap_sort(int array[], int length) {
    int i;
    
    build_max_heap(array, length);
    
    for (i = length - 1; i > 0; i--)
    {
        swap(array, 0, i);
        max_heapify(array, i, 0);
    }
}


int main () {
    int array[10] = {55, 32, 67, 28, 22, 98, 13, 26, 75, 72};
    int length = sizeof(array)/sizeof(array[0]);
    print_array(array, length);
    printf("Heap Sort\n");
    heap_sort(array, length);
    print_array(array, length);
    return 0;
}
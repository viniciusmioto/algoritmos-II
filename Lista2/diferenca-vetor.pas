program vetores;

type vetor = array [1..100] of integer;

var
    v, u : vetor;
    a, l, p: integer;

procedure ler_vetor (var v: vetor; n: integer);
var i: integer;
begin
    writeln('informe o vetor: ');
    for i:= 1 to n do
        read(v[i]);
end;

procedure imprimir_vetor(var v: vetor; n: integer);
var i: integer;
begin
    for i:= 1 to n do
        write(' | ', v[i], ' |');
    writeln();
end;

function diferenca_vetor_iterativo(var v, u: vetor; a, p, l: integer):integer;
var i, retorno: integer;
begin
    retorno := l+1;
    for i:=0 to l do
    begin
        if (v[a+i] <> u[p+i]) and (retorno = l+1) then
            retorno := i;
    end;
    diferenca_vetor_iterativo := retorno;
end;

function diferenca_vetor(var v, u: vetor; a, p, l: integer):integer;
begin
    if (a = l) or (p = l) then
        diferenca_vetor := l+1
    else
    begin    
        if (v[a] <> u[p]) then
            diferenca_vetor := a
        else
            diferenca_vetor := diferenca_vetor(v, u, a+1, p+1, l);
    end;
end;

begin
    writeln('Informe A, P e L');
    read(a, p, l);
    ler_vetor(v, l-a+1);
    ler_vetor(u, l-p+1);
    imprimir_vetor(v, l-a+1);
    imprimir_vetor(u, l-p+1);
    writeln('O primeiro elemento diferente esta em ', diferenca_vetor(v, u, a, p, l));
end.
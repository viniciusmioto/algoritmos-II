#include <stdio.h>

int binomial(int n, int k) {
    if (n < k)
        return 0;
    if (k == 0)
        return 1;
    return binomial(n-1, k-1) + binomial(n-1, k);
}

int main() {
    int n, k;
    printf("informe N: ");
    scanf("%d", &n);
    printf("informe K: ");
    scanf("%d", &k);
    printf("Binomial =  %d\n\n", binomial(n, k));
    return 0;
}

# Algoritmos e Estrutura de Dados II
---
Este repositório foi criado com o objetivo de organizar os exercícios resolvidos dessa disciplina. As atividades e programas foram realizados na linguagem _**C**_ e algumas em _**Python**_, seguindo as aulas da UFPR. É importante ressaltar que para esse curso a linguagem de programação em si não é relevante, mas sim o  algoritmo que resolve o problema em questão, por isso nos materiais dos professores utilizamos pseudo-códigos.<br />

Principais tópicos:
* Recursividade;
* Análise de Algoritmos;
* Backtracking. 

Acesse a [Página Oficial](https://www.inf.ufpr.br/cursos/ci1056/) da disciplina.

---   
Aluno: **Vinícius Mioto** <br />
Curso: **Ciência da Computação** <br />
**Universidade Federal do Paraná** <br />

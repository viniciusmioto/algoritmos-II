#include <stdio.h>

int mdc(int a, int b) {
    if (b == 0)
        return a;
    return mdc(b, a % b);
}

int mdc_it(int a, int b) {

    while ((a != 0) && (b != 0)) {
        if (a > b) {
            a = a % b;
        } else {
            b = b % a;
        }
    }
    if (b == 0)
            return a;
        return b;
}


int main() {
    int a, b;
    printf("informe A: ");
    scanf("%d", &a);
    printf("informe B: ");
    scanf("%d", & b);
    printf("o MDC de %d e %d eh  =  %d\n\n", a, b, mdc_it(a, b));
    return 0;
}

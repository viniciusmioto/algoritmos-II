#include <stdio.h>

int potencia(int x, int n) {
    if (n == 0)
        return 1;
    return x * potencia(x, n-1);
}

int main() {
    int x, n;
    printf("informe a base: ");
    scanf("%d", &x);
    printf("informe o expoente: ");
    scanf("%d", &n);
    printf("%d elevado a %d = %d\n", x, n, potencia(x, n));
    return 0;
}
#include <stdio.h>
void troca (float *a, float *b){   
    float aux;    
    aux = *a;
    *a = *b;
    *b = aux;
}

int main() {
    float x, y;
  
    printf("Insira um valor para X: ");
    scanf("%f", &x);
    printf("Insira um valor para Y: ");
    scanf("%f", &y);
    
    troca(&x,&y);
    
    printf("X é igual a %.2f e Y é igual a %.2f\n\n", x, y);
    
    return 0;
}
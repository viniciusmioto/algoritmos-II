#include <stdio.h>

void read_vector(int v[], int n) {
    int i;
    for (i = 0; i < n; i++)
        scanf("%d", &v[i]);
}

void show_vector(int v[], int n) {
    int i;
    for (i = 0; i < n; i++)
        printf("| %d ", v[i]);
    printf("|\n");
}

int min_vector(int v[], int a, int b) {
    int m;
    if (a == b) 
        return a;
    m = min_vector(v, a+1, b);
    if (v[a] < v[m])
        m = a;
    return m;
}

int main() {
    int n;
    printf("informe o tamanho do vetor: ");
    scanf("%d", &n);
    int v[n];
    read_vector(v, n);
    show_vector(v, n);
    printf("o menor valor do vetor esta na posicao: %d\n\n", min_vector(v, 0, n-1));
    return 0;
}
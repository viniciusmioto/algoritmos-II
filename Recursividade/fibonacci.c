#include <stdio.h>

int fibonacci(int n) {
    if (n <= 1)
        return 1;
    return fibonacci(n-1) + fibonacci(n-2);
}

int main() {
    int n;
    printf("informe n: ");
    scanf("%d", &n);
    printf("fibonacci de n: %d\n",  fibonacci(n));
    return 0;
}
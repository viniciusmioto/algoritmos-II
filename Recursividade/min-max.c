#include <stdio.h>

void le_vetor(int v[], int tam) {
    int i;
    for (i = 0; i < tam; i++) 
        scanf("%d", &v[i]);
    
}

void imprime_vetor(int v[], int tam) {
    int i;
    for (i = 0; i < tam; i++) 
        printf(" %d |", v[i]);
}

int min_vetor(int v[], int n) {
    if (n == 1)
        return v[0];
    else {
        int min = min_vetor(v, n-1);
        if (min < v[n-1]) 
            return min;
        return  v[n-1];
    }
}

int main() {
    int n;
    printf("informe o tamanho do vetor: ");
    scanf("%d", &n);
    int v[n];
    le_vetor(v, n);
    imprime_vetor(v, n);
    printf("o menor valor eh: %d", min_vetor(v, n));
    printf("\n");
    return 0;
}
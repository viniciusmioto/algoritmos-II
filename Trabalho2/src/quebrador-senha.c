#include "biblioteca.h"
#include "quebrador-senha.h"
#include <stdio.h>
#include <stdlib.h>

int encontrou_backtrack = 0;

void troca(char v[], int a, int b) {
    char aux = v[a];
    v[a] = v[b];
    v[b] = aux;
}

int permuta(char senha[6], int n, int encontrou) {
    int c[6];
    int i;

    for (i = 0; i < n; i ++) 
        c[i] = 0;

    if(login(senha)) {
        encontrou = 1;
        return 1;
    }

    i = 0;

    while (i < n && encontrou != 1) {
        if (c[i] < i) {
            if (i % 2 == 0) {
                troca(senha, 0, i);
            } else {
                troca(senha, c[i], i);
            }
            
            if(login(senha)) {
                encontrou = 1;
                return 1;
            }

            c[i]++;
            i = 1;

        } else {
            c[i] = 0;
            i++;
        }
    }

    return encontrou;
        
}

int permuta_backtrack(char senha[7], int l, int r) { 
    if (!encontrou_backtrack) {
        int i;
        if (l == r) {
            if (login(senha)) {
                encontrou_backtrack = 1;
                return 1;    
            }

        }
        for (i = l; i <= r; i++) { 
                troca(senha, i, l); 
                permuta_backtrack(senha, l + 1, r); 
                troca(senha, l, i); /* backtrack */ 
        }
    }
    return 0;
}

void gera_senha(char letras[], int numeros[], int inicio, int fim,
                    int index, int qtd_letras) {
    
    if (!encontrou_backtrack) {
            int list_letras[26] = {97, 98, 99, 100, 101, 102, 103, 104, 105, 106,
             107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119,
             120, 121, 122};
	    if (index == qtd_letras) { 

            char senha[7];
            senha[6] = '\0';

            int i, l, n;
            l = 0;
            n = 0;
            for(i = 0; i < 6; i++){
                if (i >= qtd_letras) {
                    senha[i] = numeros[n];
                    n++;
                } else {
                    senha[i] = letras[l];
                    l++;
                }       
            }

            permuta_backtrack(senha, 0, 5);

	    	return; 
	    } 

	    for (int i = inicio; i <= fim && fim - i + 1 >= qtd_letras - index; i++) { 
	    	letras[index] = list_letras[i]; 
	    	gera_senha(letras, numeros, i + 1, fim, index + 1, qtd_letras); 
	    } 
    }
    return;

}

void gera_numeros(int numeros[], int inicio, int fim, 
					int index, int qtd_numeros) { 
    if (!encontrou_backtrack) {
        int list_numeros[10] = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57}; 
	    if (index == qtd_numeros) { 
            int l = 6 - qtd_numeros;
            char letras[l];
            gera_senha(letras, numeros, 0, 25, 0, l);

	    	return; 
	    } 

	    for (int i = inicio; i <= fim && fim - i + 1 >= qtd_numeros - index; i++) { 
	    	numeros[index] = list_numeros[i]; 
	    	gera_numeros(numeros, i + 1, fim, index + 1, qtd_numeros); 
	    } 
    }
} 

/* Retorna 1 caso tenha encontrado a senha e 0 caso contrario */
int quebrador_senha_backtracking() {
    encontrou_backtrack = 0;

    int numeros[6];
    int qtd_numeros = 4;
    while (qtd_numeros > 0 && encontrou_backtrack == 0) {
        gera_numeros(numeros, 0, 9, 0, qtd_numeros);
        qtd_numeros--;
    }
    return encontrou_backtrack;
}


/* Retorna 1 caso tenha encontrado a senha e 0 caso contrario */
int quebrador_senha_exaustivo() {
    char senha[7];
    senha[6] = '\0';
    int n1, n2, n3, n4, l1, l2, l3, l4, encontrou;

    encontrou = 0;

    /* 4 letras e 2 números */
    for (n1 = 48; n1 <= 56; n1++) {
        for (n2 = n1 + 1; n2 <= 57; n2++) {
            for (l1 = 97; l1 <= 119; l1++) {
                for (l2 = l1 + 1; l2 <= 120; l2++) {
                    for (l3 = l2 + 1; l3 <= 121; l3++) {
                        for (l4 = l3+1; l4 <= 122; l4++) {
                            senha[0] = n1;
                            senha[1] = n2;
                            senha[2] = l1;
                            senha[3] = l2;
                            senha[4] = l3;
                            senha[5] = l4;
                            
                            if(permuta(senha, 6, encontrou)) 
                                return 1;
                        }
                    }
                }
            }
        }
    }

    /* 3 letras e 3 números */
    for (n1 = 48; n1 <= 55; n1++) {
        for (n2 = n1 + 1; n2 <= 56; n2++){
            for (n3 = n2 + 1; n3 <= 57; n3++) {
                for (l1 = 97; l1 <= 120; l1++) {
                    for (l2 = l1 + 1; l2 <= 121; l2++) {
                        for (l3 = l2 + 1; l3 <= 122; l3++) {
                            senha[0] = n1;
                            senha[1] = n2;
                            senha[2] = n3;
                            senha[3] = l1;
                            senha[4] = l2;
                            senha[5] = l3;
                            
                            if(permuta(senha, 6, encontrou)) 
                                return 1;
                            
                        }
                    }
                }
            }
        }
    }

    /* 2 letras e 4 números */
    for (n1 = 48; n1 <= 54; n1++) {
        for (n2 = n1 + 1; n2 <= 55; n2++) {
            for (n3 = n2 + 1; n3 <= 56; n3++) {
                for (n4 = n3 + 1; n4 <= 57; n4++) {
                    for (l1 = 97; l1 <= 121; l1++) {
                        for (l2 = l1 + 1; l2 <= 122; l2++) {
                            senha[0] = n1;
                            senha[1] = n2;
                            senha[2] = n3;
                            senha[3] = n4;
                            senha[4] = l1;
                            senha[5] = l2;
                            
                            if(permuta(senha, 6, encontrou)) 
                                return 1;
                                
                        }
                    }
                }
            }
        }
    }

    return encontrou;
    
}

/* inclui repeticoes de caracteres

int super_exaustivo() {
    int c0, c1, c2, c3, c4, c5;
    char senha[7];
    senha[6] = '\0';

    c0 = 48;
    while (c0 <= 122) {
        if (c0 == 58)
            c0 = 97;
        
        c1 = 48;
        while (c1 <= 122) {
            
            if (c1 == 58)
                c1 = 97;

            c2 = 48;
            while (c2 <= 122) {
                
                if (c2 == 58)
                    c2 = 97;

                c3 = 48;
                while (c3 <= 122) {
                    
                    if (c3 == 58)
                        c3 = 97;

                    c4 = 48;
                    while (c4 <= 122) {
                        
                        if (c4 == 58)
                            c4 = 97;

                        c5 = 48;
                        while (c5 <= 122) {
                            
                            if (c5 == 58)
                                c5 = 97;

                            senha[0] = c0;
                            senha[1] = c1;
                            senha[2] = c2;
                            senha[3] = c3;
                            senha[4] = c4;
                            senha[5] = c5;
                            
                            if (login(senha))
                                return 1;
                            c5++;
                        }
                        c4++;
                    }
                    c3++;
                }
                c2++;
            }
            c1++;
        }
        c0++;
    }
    return 0;
}

*/                   
                
                                

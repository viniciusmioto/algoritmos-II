#include "particiona.h"
#include "biblioteca.h"

/* -------------------------------------------------------------------------- */
/* devolve a mediana de a, b e c                                              */

static int mediana(int a, int b, int c) {
  int compab = compara(a,b);

  if ((compab <= 0) && (compara(b,c) <= 0))
    return b;
  if ((compab >= 0) && (compara(a,c) <= 0))
    return a;
  return c;
}

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* ordena v[a..b]  usando o algoritmo  "QuickSort com mediana de  3" e
   devolve v                                                                  */


int *quicksort_mediana(int v[], int a, int b) {
    if (a < b) {
      int s1 = sorteia(a,b);
      int s2 = sorteia(a,b);
      int s3 = sorteia(a,b);

      int m = mediana(v[s1],v[s2],v[s3]);

      if (compara(m,v[s1]) == 0)
        troca(v,s1,b);
      else if (compara(m,v[s2]) == 0)
        troca(v,s2,b);
      else
        troca(v,s3,b);

      int p = particiona(v,a,b,v[b]);

      quicksort_mediana(v, a, p-1);
      quicksort_mediana(v, p+1, b);
    }
    return v;
}
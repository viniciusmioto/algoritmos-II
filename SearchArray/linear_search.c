#include <stdio.h>

/* return the position of the element, if it exists */
int find(int x, int array[10], int start, int position) {
    if (start > position)
        return -1;
    
    if (x == array[position])
        return position;

    return search(x, array, start, position -1); 
}

/* return the position that the element should be */
int search(int x, int array[10], int start, int position) {
    if (start > position)
        return start-1;
    
    comparisons++;
    if (x >= array[position])
        return position;

    return search(x, array, start, position -1); 
}

int main() {
    int x, start, end, position;
    int array[10] = {2, 0, 1, 0, 3, 9, 3, 1, 5, 6};
    x = 3;
    start = 0;
    end = 6;
    position = search(x, array, start, end);
    if (position == -1) {
        printf("value not found! \n");
        return 0;
    } 
    printf("found at %dº position \n", position);
    return 1;
}

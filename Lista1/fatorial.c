#include <stdio.h>

int fatorial(int n) {
    if (n == 1) return 1;
    
    return n * fatorial(n-1);
}

int main(){
    int n;
    printf("digite um número: ");
    scanf("%d", &n);
    printf("o fatorial de %d é:  %d \n", n, fatorial(n));
    return 0;
}
#include <stdio.h>

int fatorial(int n) {
    if (n == 1) 
        return 1;
    return n * fatorial(n-1);

}

int main() {
    int n;
    printf("digite um numero: ");
    scanf("%d", &n);
    printf("%d! = ", n);
    printf("%d \n\n", fatorial(n));

    return 0;
}
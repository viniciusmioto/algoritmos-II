#include <stdio.h>

int expo(int x, int n) {
    if (n == 0) return 1;

    return x * expo(x, n-1);
}

void le_vetor(int v[], int n) {
    printf("informe o vetor:\n");
    int i;
    for (i = 0; i < n; i++) 
        scanf("%d", &v[i]);
}

void imprime_vetor(int v[], int n) {
    int i;
    for (i = 0; i < n; i++) 
        printf("| %d ", v[i]);
    printf("|\n\n");
}

int polinomio(int v[], int a, int b, int x) {
    if (x == 0)
        return 0;
    if (a >= b)
        return v[a];
    return polinomio(v, a, b-1, x) + expo(x, b) * v[b];
}

int main() {
    int n, x;
    printf("qual o tamanho do vetor:  ");
    scanf("%d", &n);
    int v[n];
    le_vetor(v, n);
    imprime_vetor(v, n);
    printf("informe x: ");
    scanf(" %d", &x); 
    printf("\nResultado =  %d \n", polinomio(v, 0, n-1, x));
    return 0;
}
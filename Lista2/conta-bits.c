#include <stdio.h>

int bits_um(int n) {
    if (n == 0) 
        return 0;
    return n % 2 + bits_um(n / 2);
}

int quantidade_bits(int n) {
    if (n == 0)
        return 0;
    return quantidade_bits(n / 2) + 1;
}

int main() {
    int n;
    printf("informe N: ");
    scanf("%d", &n);
    printf("Quantidade de bits 1 na representacao binaria =  %d\n\n", bits_um(n));
    printf("Quantidade total de bits = %d\n\n", quantidade_bits(n));
    return 0;
}
#include <stdio.h>
#include <math.h>

void read_matrix(int matrix[10][10], int size) {
    int line, column;
    printf("enter matrix:\n");
    for (line = 0; line < size; line++)
        for(column = 0; column < size; column++)
            scanf("%d", &matrix[line][column]);
}

void print_matrix(int matrix[10][10], int size) {
    int line, column;
    for (line = 0; line < size; line++) {
        for(column = 0; column < size; column++)
            printf(" | %d", matrix[line][column]);
        printf(" |\n");
    }
    printf("\n");
}

void paint_matrix(int matrix[10][10], int i, int  j, int n) {
    int m;
    if (n <= 2) {
        matrix[i][j] = 1;
        matrix[i][j+1] = 0;
        matrix[i+1][j] = 0;
        matrix[i+1][j+1] = 1;
    } else {
        m = floor(n/2);
        paint_matrix(matrix, i, j, m);
        paint_matrix(matrix, i, j+m, m);
        paint_matrix(matrix, i+m, j, m);
        paint_matrix(matrix, i+m, j+m, m);
    }
    
}

int main() {
    int size;
    printf("enter size: ");
    scanf("%d", &size);
    int matrix[size][size];
    read_matrix(matrix, size);
    paint_matrix(matrix, 0, 0, size);
    print_matrix(matrix, size);
    return 0;
}
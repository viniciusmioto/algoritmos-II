#include "particiona.h"

/* -------------------------------------------------------------------------- */
/* ordena v[a..b] usando o algoritmo QuickSort e devolve v */

int *quicksort(int v[], int a, int b) {
  if (a < b) {
    int p = particiona(v,a,b,v[b]);

    quicksort(v,a,p-1);
    quicksort(v,p+1,b);
  }
  return v;
}
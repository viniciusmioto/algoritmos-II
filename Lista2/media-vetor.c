#include <stdio.h>

void le_vetor(int v[], int n) {
    printf("informe o vetor:\n");
    int i;
    for (i = 0; i < n; i++) 
        scanf("%d", &v[i]);
}

void imprime_vetor(int v[], int n) {
    int i;
    for (i = 0; i < n; i++) 
        printf("| %d ", v[i]);
    printf("|\n\n");
}

float media_vetor(int v[], int a, int b) {
    int n = b - a + 1;
    if (n <= 1)
        return v[0];
    return ((media_vetor(v, a, b-1) * (n-1)) + v[b]) / n;
}

int main() {
    int n;
    printf("qual o tamanho do vetor:  ");
    scanf("%d", &n);
    int v[n];
    le_vetor(v, n);
    printf("a media do vetor eh:  %.2f\n\n", media_vetor(v, 0, n-1));
    return 0;
}
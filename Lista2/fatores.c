#include <stdio.h>

void fatores_it(int n) {
    int i;
    for (i = 1; i <= n; i++) {
        if ((n % i) == 0) {
            printf(" %d ", i);
        }
    }
}

void fatores_re(int n, int i) {
    if (i <= n) {
        if (n % i == 0)
            printf(" %d ", i);
        fatores_re(n, i+1);
    }
}

int main() {
    int n, i;
    i = 1;
    printf("informe um numero: ");
    scanf("%d", &n);
    fatores_it(n);
    printf("\n\n");
    return 0;
}
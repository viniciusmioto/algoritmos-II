from collections import deque
 
 
# Function to generate a power subset of given subset `set`
def findPowerSet(set, subset, n, x):
 
    # if we have considered all elements
    if n == 0:
        if (sum(subset) == x):
            print(subset)
        return
 
    if sum(subset) + set[n-1] <= x:
        # consider the n'th element
        subset.append(set[n - 1])
        findPowerSet(set, subset, n - 1, x)
 
    else:
        # or don't consider the n'th element
        subset.pop()
        findPowerSet(set, subset, n - 1, x)
 
 
if __name__ == '__main__':
 
    set = [5, 6, 1, 2, 3]
 
    subset = []
    findPowerSet(set, subset, len(set), 10)
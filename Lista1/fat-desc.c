#include <stdio.h>

int fatdesc(int n, int k) {
    int i = n - k + 1;
    if (k <= 1)
        return n;
    return i * fatdesc(n, k-1);
}

int main () {
    int n, k;
    printf("informe N: ");
    scanf("%d", &n);
    printf("informe K: ");
    scanf("%d", & k);
    printf("O fatorial de %d a %d é:  %d\n", n, k, fatdesc(n, k));
    return 0;
}
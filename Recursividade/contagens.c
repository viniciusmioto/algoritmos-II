#include <stdio.h>

void count_down(int n) {
    if (n == 1)
        printf(" %d", n);
    else {
        printf(" %d", n);
        count_down(n-1);
    }
}

void count_up(int n, int z) {
    if (n > z) {
        printf("%d ", z);
        count_up(n, z+1);
    }
        
    else {
        printf("%d ", z);
    }
}

void count_down_up(int n) {
    if (n == 1) 
        printf(" %d", n);
    else {
        printf(" %d", n);
        count_down_up(n-1);
        printf(" %d", n);
    }
}

int main () {
    int n;
    int z = 0;
    printf("informe um numero: ");
    scanf("%d", &n);
    count_up(n, z);
    printf("\n");
}
#include <stdio.h>

void read_vector(int v[], int n) {
    int i;
    for (i = 0; i < n; i++)
        scanf("%d", &v[i]);
}

void show_vector(int v[], int n) {
    int i;
    for (i = 0; i < n; i++)
        printf("| %d ", v[i]);
    printf("|\n");
}

int search_vector(int x, int v[], int a, int b) {
    int r;
    if (a > b) 
        return -1;
    r = search_vector(x, v, a, b-1);
    if (r != -1) 
        return r;
    if (x == v[b])
        return b;
    return -1;
}

int main() {
    int n, x;
    printf("informe o tamanho do vetor: ");
    scanf("%d", &n);
    int v[n];
    read_vector(v, n);
    show_vector(v, n);
    printf("informe o valor a ser pesquisado: ");
    scanf("%d", &x);
    printf("o valor pesquisado esta na posicao:  %d\n\n", search_vector(x, v, 0, n-1));
    return 0;
}
program transposta;

type
    matriz = array[1..10, 1..10] of integer;

var
    m: matriz;
    tamanho: integer;

procedure troca(var a, b: integer);
var aux: integer;
begin
    aux := a;
    a := b;
    b := aux;
end;

procedure transpor(var m: matriz; tamanho: integer);
var i: integer;
begin
    if tamanho <= 1 then
        m[tamanho][tamanho] := m[tamanho][tamanho]
    else
    begin
        for i := 1 to tamanho do   
            troca(m[i][tamanho], m[tamanho][i]);
        transpor(m, tamanho-1);    
    end;
end;

procedure ler_matriz(var m: matriz; tamanho: integer);
var l, c: integer;
begin
    for l := 1 to tamanho do
        for c := 1 to tamanho do
            read(m[l][c]);
end;

procedure imprimir_matriz(m: matriz; tamanho: integer);
var l, c: integer;
begin
    for l := 1 to tamanho do
    begin
        for c := 1 to tamanho do
            write(' | ', m[l][c]);
        writeln(' | ');
    end;
end;

begin
    write('informe tamanho:  ');
    read(tamanho);
    ler_matriz(m, tamanho);
    imprimir_matriz(m, tamanho);
    transpor(m, tamanho);
    writeln();
    writeln();
    imprimir_matriz(m, tamanho);
end.
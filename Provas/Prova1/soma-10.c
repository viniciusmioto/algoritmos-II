#include <stdio.h>
#include <math.h>

void read_array(int array[], int size) {
    int i;
    for (i = 0; i < size; i++)
        scanf("%d", &array[i]);
}

void write_array(int array[], int size) {
    int i;
    for (i = 0; i < size; i++)
        printf(" | %d", array[i]);
    printf(" |\n\n");
}

int array_10(int array[], int a, int b) {
    if (a == b) {
        if (array[a] == 10)
            return 1;
        else
            return 0;
    }
    if (a < b) {
        if (array[a] + array[b] == 10)
            return array_10(array, a+1, b-1);
        else
            return 0;
    }
    return 1;
}

int main() {
    int size;
    printf("enter the size of the array: ");
    scanf("%d", &size);
    int array[size];
    read_array(array, size);
    write_array(array, size);
    if (array_10(array, 0, size-1) == 1)
        printf("YES");
    else
        printf("NO");
    printf("\n\n");
    return 0;
}
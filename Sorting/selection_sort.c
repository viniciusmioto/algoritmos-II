#include <stdio.h>

void swap(int array[], int a, int b) {
    int aux;
    aux = array[a];
    array[a] = array[b];
    array[b] = aux;
}

int min_array(int array[], int start, int end) {
    int min;

    if (start == end)
        return start;
    
    min = min_array(array, start, end - 1);

    if (array[end] < array[min])
        min = end;
    return min;
}

void print_array(int array[], int n) {
    int i;
    for (i = 0; i < n; i++)
        printf("| %d ", array[i]);

    printf("|\n");
}

void selection_sort(int array[], int start, int end) {
    if (start >= end)
        return ;
    swap(array, start, min_array(array, start, end));
    return selection_sort(array, start + 1, end);
}

int main () {
    int array[10] = {55, 32, 67, 28, 22, 98, 13, 26, 75, 72};
    int length = sizeof(array)/sizeof(array[0]);
    print_array(array, length);
    printf("Selection Sort\n");
    selection_sort(array, 0, length - 1);
    print_array(array, length);
    return 0;
}
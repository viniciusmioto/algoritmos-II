#include <stdio.h>
#include <math.h>

void read_array(int array[], int size) {
    int i;
    for (i = 0; i < size; i++)
        scanf("%d", &array[i]);
}

void write_array(int array[], int size) {
    int i;
    for (i = 0; i < size; i++)
        printf(" | %d", array[i]);
    printf(" |\n\n");
}


int min_array(int array[], int first, int last) {
    int middle, min_left, min_right;
    if (first == last)
        return first;
    middle = floor((first + last) / 2);
    min_left = min_array(array, first, middle);
    min_right = min_array(array, middle + 1, last);
    if (array[min_left] <= array[min_right])
        return min_left;
    return min_right;
}

/*
int min_array(int array[], int first, int last) {
    int min;
    if (first == last)
        return first;
    min = min_array(array, first, last-1);
    if (array[last] <= array[min])
        min = last;
    return min;
}
*/

int main() {
    int size;
    printf("enter the size of the array: ");
    scanf("%d", &size);
    int array[size];
    read_array(array, size);
    write_array(array, size);
    printf("The minimum value of the array is in the %dº position\n\n", min_array(array, 0, size-1)+1);
    return 0;
}
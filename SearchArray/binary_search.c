#include<stdio.h>
#include <math.h>

int binary_search(int x, int array[], int start, int end) {
    int m;

    if (start > end)
        return -1;
    m = floor((start + end)/2);

    if (x == array[m])
        return m;

    if (x < array[m])
        return binary_search(x, array, start, m - 1);
    return binary_search(x, array, m + 1, end);
}

int main () {
    int array[10] = {23, 47, 48, 52, 57, 65, 78, 79, 86, 100};
    int position, x;
    x = 52;
    position = binary_search(x, array, 0, 9);
    if (position == -1) {
        printf("value not found! \n");
        return 0;
    } 
    printf("found at %dº position \n", position);
    return 1;
}
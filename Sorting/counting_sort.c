#include <stdio.h>

void print_array(int array[], int start, int end) {
    int i;
    for (i = start; i <= end; i++)
        printf("| %d ", array[i]);

    printf("|\n");
}

void counting_sort(int array[], int start, int end, int k) {
    int counter[k+1], sorted_array[end + 1], i;
    
    for (i = 0; i < k; i++)
        counter[i] = 0;

    for (i = 0; i <= end; i++) 
        counter[array[i]]++;

    for (i = 1; i <= k; i++) 
        counter[i] = counter[i] + counter[i - 1];

    for (i = end; i >= 0; i--) {
        sorted_array[counter[array[i]]-1] = array[i];
        counter[array[i]] = counter[array[i]] - 1;
    }

    for (i = 0; i <= end; i++)
        array[i] = sorted_array[i];
}

int main() {
    int array[8] = {2, 0, 2, 0, 3, 9, 3, 1};
    int length = sizeof(array)/sizeof(array[0]);
    int max = 9;
    print_array(array, 0, length - 1);
    printf("Counting Sort\n");
    counting_sort(array, 0, length - 1, max);
    print_array(array, 0, length - 1);
    return 0;
}
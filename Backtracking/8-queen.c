#include <stdbool.h> 
#include <stdio.h> 

void print_solution(int board[8][8]) {
    int i, j;
    
    for (i = 0; i <= 7; i++) {
        for (j = 0; j <= 7; j++)
            printf(" %d ", board[i][j]);
        printf("\n");
    }
}

bool verify_position(int board[8][8], int row, int col) {
    int i, j;
    
    /* verify column */ 
    for (i = 0; i <= 7; i++) {
        if (board[i][col] == 1) 
            return false;
    }

    /*verify upper diagonal*/
    i = row - 1;
    j = col - 1;
    while(i >= 0 && j >= 0) {
        if (board[i][j] == 1)
            return false;
        i--;
        j--;
    }

    i = row + 1;
    j = col + 1;
    while(i <= 7 && j <= 7) {
        if (board[i][j] == 1)
            return false;
        i++;
        j++;
    }
    
    /*verify lower diagonal*/
    i = row - 1;
    j = col + 1;
    while(i >= 0 && j <= 7) {
        if (board[i][j] == 1)
            return false;
        i--;
        j++;
    }

    i = row + 1;
    j = col - 1;
    while(i <= 7 && j >= 0) {
        if (board[i][j] == 1)
            return false;
        i++;
        j--;
    }

    return true;
}

bool find_solution(int board[8][8], int row) {
    if (row > 7) {
        return true;
    }

    int position;
    for(position = 0; position <= 7; position++) {
        /* check if the queen can be placed */
        if (verify_position(board, row, position)) {
            print_solution(board);
            printf("\n\n");
            board[row][position] = 1;
        
            if (find_solution(board, row + 1))
                return true;

            board[row][position] = 0; /* BACKTRACK */
        }
    }

    return false;
}

bool solve_board() {
    int board[8][8] = {
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0}
    };

    if (find_solution(board, 0) == false) {
        printf("solution does not exist");
        return false;
    }

    print_solution(board);
    return true;
}

int main() {
    solve_board();
    return 0;
}

#include <stdio.h> 

int potencia(int r, int n){
    if(n == 1) return r; 
    return r * potencia(r, n-1);
}

int geometrica(int a1, int r, int n){
    int pot;

    if(n == 1) return a1; 

    pot = potencia(r, n-1); 
    return a1 * pot + geometrica(a1, r, n-1);
}  

int main(){
    int a1; 
    int r; 
    int n; 

    scanf("%d", &a1);
    scanf("%d", &r);
    scanf("%d", &n); 

    printf("%d \n", geometrica(a1, r, n)); 
  
} 

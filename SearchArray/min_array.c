#include <stdio.h>

int min_array(int array[10], int start, int end) {
    int min;

    if (start == end)
        return start;
    
    min = min_array(array, start, end - 1);

    if (array[end] < array[min])
        min = end;
    return min;
}

int main () {
    int array[10] = {4, 2, 6, 7, 8, 9, 1, 3, 4, 5};
    printf("the min value in array is in %dº postion\n", min_array(array, 0, 9));
    return 0;
}


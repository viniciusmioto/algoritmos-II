#include <stdio.h>

void le_vetor(int v[], int n) {
    printf("informe o vetor:\n");
    int i;
    for (i = 0; i < n; i++) 
        scanf("%d", &v[i]);
}

void imprime_vetor(int v[], int n) {
    int i;
    for (i = 0; i < n; i++) 
        printf("| %d ", v[i]);
    printf("|\n\n");
}

int palindromo(int v[], int a, int b) {
    if (a >= b) {
        return 1;
    }
    return palindromo(v, a + 1, b - 1) * (v[a] == v[b]);
}

int main() {
    int n;
    printf("qual o tamanho do vetor:  ");
    scanf("%d", &n);
    int v[n];
    le_vetor(v, n);
    imprime_vetor(v, n);
    if (palindromo(v, 0, n-1) == 1)
        printf("SIM\n");
    else
        printf("NAO\n");
    return 0;
}
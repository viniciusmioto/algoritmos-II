# include <stdio.h>
# include <math.h>

void print_array(int array[], int start, int end) {
    int i;
    for (i = start; i <= end; i++)
        printf("| %d ", array[i]);

    printf("|\n");
}

void copy_array(int array[], int aux_array[], int start, int end) {
    int i, k;
    i = start;
    for (k = 0; k <= end - start; k++, i++) {
        array[i] = aux_array[k];
    }
}

void merge(int array[], int start, int middle, int end) {
    int first_half, second_half, k;
    int aux_array[end + 1];

    if (start >= end) {
        return ;
    }

    first_half = start;
    second_half = middle + 1;

    for (k = 0; k <= end - start; k++) {
        if (second_half > end || (first_half <= middle && array[first_half] <= array[second_half])) {
            aux_array[k] = array[first_half];
            first_half++;
        } else {
            aux_array[k] = array[second_half];
            second_half++;
        }
    }
    
    return copy_array(array, aux_array, start, end);
}

void merge_sort(int array[], int start, int end) {
    if (start >= end)
        return ;

    int middle =  floor((start + end) / 2);

    merge_sort(array, start, middle);
    merge_sort(array, middle + 1, end);
    return merge(array, start, middle, end);

}

int main () {
    int array[10] = {55, 32, 67, 28, 22, 98, 13, 26, 75, 72};
    int length = sizeof(array)/sizeof(array[0]);
    print_array(array, 0, length - 1);
    printf("Merge Sort\n");
    merge_sort(array, 0, length - 1);
    print_array(array, 0, length - 1);
    return 0;
}

/* 

iterative merge

void merge(int array[], int start, int middle, int end) {
    int i, j, k;
    int aux_array[end + 1];

    for (i = start; i <= middle; i++)
        aux_array[i] = array[i];

    for (j = middle; j <= end; j++)
        aux_array[end + middle - j + 1] = array[j];

    i = start;
    j = end;

    for (k = start; k <= end; k++) {
        if (aux_array[i] <= aux_array[j]) {
            array[k] = aux_array[i];
            i++;
        } else {
            array[k] = aux_array[j];
            j--;
        }
    }
}

descending sort

void merge(int array[], int start, int middle, int end) {
    int first_half, second_half, k;
    int aux_array[end + 1];

    if (start >= end) {
        return ;
    }

    first_half = start;
    second_half = middle + 1;

    for (k = 0; k <= end - start; k++) {
        if (second_half > end || (first_half <= middle && array[first_half] >= array[second_half])) {
            aux_array[k] = array[first_half];
            first_half++;
        } else {
            aux_array[k] = array[second_half];
            second_half++;
        }
    }
    
    return copy_array(array, aux_array, start, end);
}

*/

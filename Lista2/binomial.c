#include <stdio.h>

int fatorial(int n) {
    if (n == 1) return 1;
    
    return n * fatorial(n-1);
}

int binomial(int n, int k) {
    if (n < k)
        return 0;
    return fatorial(n)/(fatorial(k)*fatorial(n-k));
}

int main() {
    int n, k;
    printf("informe N: ");
    scanf("%d", &n);
    printf("informe K: ");
    scanf("%d", &k);
    printf("Binomial =  %d\n\n", binomial(n, k));
    return 0;
}
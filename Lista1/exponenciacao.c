#include <stdio.h>

int expo(int x, int n) {
    if (n == 1) return x;

    return x * expo(x, n-1);
}

int main() {
    int x, n;
    printf("digite um numero:  ");
    scanf("%d", &x);
    printf("digite uma potencia: ");
    scanf("%d", &n);
    printf("%d elevado a %d  =  %d \n", x, n, expo(x, n));
    return 0;
}

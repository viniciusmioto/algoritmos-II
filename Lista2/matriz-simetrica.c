#include <stdio.h>

void read_matrix(int matrix[10][10], int size) {
    int line, column;
    printf("enter matrix:\n");
    for (line = 0; line < size; line++)
        for(column = 0; column < size; column++)
            scanf("%d", &matrix[line][column]);
}

void print_matrix(int matrix[10][10], int size) {
    int line, column;
    for (line = 0; line < size; line++) {
        for(column = 0; column < size; column++)
            printf(" | %d", matrix[line][column]);
        printf(" |\n");
    }
    printf("\n");
}

int checkSimetry(int matrix[10][10], int size, int start) {
    if (start == size - 1)
        return 1;
    if (matrix[size - 1][start] != matrix[start][size -1])
        return 0;
    return checkSimetry(matrix, size, start + 1);
}

int simetricMatrix(int matrix[10][10], int size) {
    int simetric;
    if (size == 1)
        return 1;
    simetric = checkSimetry(matrix, size, 0);
    if (simetric * simetricMatrix(matrix, size -1) == 1)
        return 1;
    return 0;
}

int main() {
    int size;
    printf("enter size: ");
    scanf("%d", &size);
    int matrix[size][size];
    read_matrix(matrix, size);
    print_matrix(matrix, size);
    if (simetricMatrix(matrix, size)) 
        printf("SIMETRIC\n\n");
    else
        printf("NOT simetric\n\n");
    return 0;
}
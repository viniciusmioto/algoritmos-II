#include <stdio.h>
#include <math.h>

int expo(int a, int b) {
    if (b == 0)
        return 1;
    if (b % 2 == 0)
        return expo(a*a, ceil(b/2));
    return expo(a*a, ceil(b/2)) * a;
}


int main() {
    int a, b;
    printf("informe A: ");
    scanf("%d", &a);
    printf("informe B: ");
    scanf("%d", &b);
    printf("Saida =  %d\n\n", expo(a, b));
    return 0;
}

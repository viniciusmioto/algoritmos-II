#include <stdio.h>

void le_vetor(int v[], int n) {
    printf("informe o vetor:\n");
    int i;
    for (i = 0; i < n; i++) 
        scanf("%d", &v[i]);
}

void imprime_vetor(int v[], int n) {
    int i;
    for (i = 0; i < n; i++) 
        printf("| %d ", v[i]);
    printf("|\n\n");
}

void inverte_vetor(int v[], int a, int b) {
    int aux;
    if (b > a) {    
        aux = v[a];
        v[a] = v[b];
        v[b] = aux;
        inverte_vetor(v, a+1, b-1); 
    }
}

int main() {
    int n;
    printf("qual o tamanho do vetor:  ");
    scanf("%d", &n);
    int v[n];
    le_vetor(v, n);
    inverte_vetor(v, 0, n-1);
    imprime_vetor(v, n);
    return 0;
}
#include <stdio.h>

int f (int n) {
    if (n > 100) 
        return n - 10;
    return f(f(n+11));
}

int main() {
    int n;
    printf("informe um numero: ");
    scanf("%d", &n);
    printf("saída: %d \n", f(n));
    return 0;
}
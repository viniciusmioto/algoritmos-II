#include <stdio.h>

int mult(int n, int m) {
    if (m == 1)
        return n ;
    if (m == -1)
        return n * -1;
    if (m <0)
        return mult(n, m+1) - n;
    return mult(n, m-1) + n;
}

int main() {
    int n, m;
    printf("informe N: ");
    scanf("%d", &n);
    printf("informe M: ");
    scanf("%d", & m);
    printf("%d * %d  =  %d\n\n", n, m, mult(n, m));
    return 0;
}
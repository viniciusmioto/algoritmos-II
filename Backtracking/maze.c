#include <stdbool.h>
#include <stdio.h>

void print_solution(int solution[7][6]) {
    int i, j;
    for (i = 0; i <7; i ++){
        for(j = 0; j < 6; j++)
            printf(" %d ", solution[i][j]);
        printf("\n");
    }
    printf("\n");
}

bool exit_found(int maze[7][6], int x, int y, int solution[7][6]) {
    if (x == 7 || y == 6)
        return true;
    return false;
}

bool safe_position(int maze[7][6], int x, int y) {
    if (x >= 0 && x <= 7 && y >= 0 && y <= 6 && maze[x][y] == 0)
        return true;
    return false;
}

bool find_exit(int maze[7][6], int x, int y, int solution[7][6]) {
    if (exit_found(maze, x, y, solution))
        return true;

    if (safe_position(maze, x, y)) {
        if (solution[x][y] == 1) 
            return false;

        solution[x][y] = 1;

     /* up */
    if (find_exit(maze, x-1, y, solution))        
        return true;

    /* down */
    if (find_exit(maze, x+1, y, solution))        
        return true;
    
    /* right */
    if (find_exit(maze, x, y+1, solution))        
        return true;
    
    /* left */
    if (find_exit(maze, x, y-1, solution))        
        return true;

    solution[x][y] = 0;
    return false;
    
    }
    return false;
}

bool solve_maze(int maze[7][6]) {
    int x = 1;
    int y = 1;

    int solution[7][6] = {
        {0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0},
    };

    if (find_exit(maze, x, y, solution) == false) {
        printf("solution does not exist");
        return false;
    }
    printf("\n exit found\n\n");
    print_solution(solution);
    return true;
}


int main () {
    int maze[7][6] = {
        {1, 1, 1, 1, 1, 1},
        {1, 0, 0, 0, 0, 1},
        {1, 0, 1, 1, 0, 1},
        {1, 0, 0, 0, 1, 1},
        {1, 0, 1, 1, 1, 1},
        {1, 0, 0, 0, 0, 1},
        {1, 1, 1, 1, 0, 1},
        };

    solve_maze(maze);
    return 0;
}
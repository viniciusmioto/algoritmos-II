# include <stdio.h>
# include <math.h>

int binary_search(int x, int array[], int start, int end) {
    int m;

    if (start > end)
        return end;
    m = floor((start + end)/2);

    if (x == array[m])
        return m;

    if (x < array[m])
        return binary_search(x, array, start, m - 1);
    return binary_search(x, array, m + 1, end);
}

void print_array(int array[], int n) {
    int i;
    for (i = 0; i < n; i++)
        printf("| %d ", array[i]);

    printf("|\n");
}

void swap(int array[], int a, int b) {
    int aux;
    aux = array[a];
    array[a] = array[b];
    array[b] = aux;
}

void insert(int array[], int start, int position) {
    int true_position, i;
    true_position = binary_search(array[position], array, start, position - 1);
    for (i = position; i > true_position + 1; i--) 
        swap(array, i, i-1);
    return ;
}

void insertion_sort(int array[], int start, int position) {
    if (start >= position) 
        return ;
    insertion_sort(array, start, position - 1);
    insert(array, start, position);
    return ;
}

int main () {
    int array[10] = {55, 32, 67, 28, 22, 98, 13, 26, 75, 72};
    int length = sizeof(array)/sizeof(array[0]);
    print_array(array, length);
    printf("Insertion Sort\n");
    insertion_sort(array, 0, length - 1);
    print_array(array, length);
    return 0;
}